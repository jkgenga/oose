class Num{
  //n is assumed to be a positive number
  final long n;
  Num(long n){
    assert n>=0;
    this.n = n;
  }
  
  String toBin(){
    return leadingZero(Long.toBinaryString(n));
  }

  String toOct(){
    return leadingZero(Long.toOctalString(n));
  }

  String toHex(){
    return leadingZero(Long.toHexString(n));
  }
  String toBase(int b){
    switch (b) {
      case 2: return toBin();
      case 8: return toOct();
      case 16: return toHex();
      default: return "unknown base";
    }
  }

  private String leadingZero(String str) {
    if (!str.startsWith("0")) {
      return "0" + str;
    } else return str;
  }

  static char getDigit(long d){
    return (char) (d<10? '0'+d : 'A'+d-10);
  }

  public static void main(String[] args) {
    Num num = new Num(23);
    System.out.println(num.toBase(2));
    System.out.println(num.toBin());
    System.out.println(num.toBase(8));
    System.out.println(num.toOct());
    System.out.println(num.toBase(16));
    System.out.println(num.toHex());
  }
}