class Fib {
  int step = 0;

  static int fib0(int n) {
    if (n == 0) {
      return 0;
    } else if (n == 1) {
      return 1;
    } else {
      return fib0(n - 2) + fib0(n - 1);
    }
  }

  static long fib1(int n) {
    return (int) (
        ( Math.pow((1 + Math.sqrt(5)) / 2, n) - 
          Math.pow((1 - Math.sqrt(5)) / 2, n)
        ) / 
        Math.sqrt(5)
      );
  }
  static long fib(int n) {
    double numerator1 = Math.pow((1 + Math.sqrt(5)) / 2, n);
    double numerator2 = Math.pow((1 - Math.sqrt(5)) / 2, n);
    double numerator = numerator1 - numerator2;
    double denominator =  Math.sqrt(5);
    return (int) (numerator / denominator);
  }

  long nextFib(){
    return fib(step++);
  }
  
  public static void main(String[] args){
    Fib fib = new Fib();
    for (int i=0;i <= 10; i = i+1){
      System.out.println(fib.nextFib());
    }
  }

  public static void main1(String args[]) {
    int x = 0;
    System.out.println("fib " + x + " = " + fib(x));
    x++;
    System.out.println("fib " + x + " = " + fib(x));
    x++;
    System.out.println("fib " + x + " = " + fib(x));
    x++;
    System.out.println("fib " + x + " = " + fib(x));
    x++;
    System.out.println("fib " + x + " = " + fib(x));
    x++;
    System.out.println("fib " + x + " = " + fib(x));
    x++;
    System.out.println("fib " + x + " = " + fib(x));
    x++;
    System.out.println("fib " + x + " = " + fib(x));
    x++;
    System.out.println("fib " + x + " = " + fib(x));
    x++;
    System.out.println("fib " + x + " = " + fib(x));
    x++;
    System.out.println("fib " + x + " = " + fib(x));
    x++;
    System.out.println("fib " + x + " = " + fib(x));
    x++;
    System.out.println("fib " + x + " = " + fib(x));
    x++;
    System.out.println("fib " + x + " = " + fib(x));
    x++;
    System.out.println("fib " + x + " = " + fib(x));
    x++;
    System.out.println("fib " + x + " = " + fib(x));
    x++;
    System.out.println("fib " + x + " = " + fib(x));
  }
}
