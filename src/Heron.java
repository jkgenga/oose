class Heron{

  double x1 = 1.0;

  static double sqrt(double x){
    assert x>=0;
    return sqrt(x , (x + 1) / 2) ;
  }

  static double sqrt0(double x, double approx) {
    double xn = 1;
    for (int i = 0; i < 5; i ++) {
      xn = (xn + approx) / 2; 
      approx = x / xn;
    }
    return approx;
  }
  static double sqrt(double x, double approx) {
    return sqrtRecursiv(x, 1, approx, 5);
  }

  static double sqrtRecursiv(double x, double xn, double approx, int steps) {
    if (steps > 0) {
      xn = (xn + approx) / 2;
      return sqrtRecursiv(x, xn, x / xn, steps - 1);
    } else 
    return approx;
  }

  public static void main(String[] args) {
    double x = 1.0;
    System.out.println("Wurzel von +" + (int)x + " = " + sqrt(x++));
    System.out.println("Wurzel von +" + (int)x + " = " + sqrt(x++));
    System.out.println("Wurzel von +" + (int)x + " = " + sqrt(x++));
    System.out.println("Wurzel von +" + (int)x + " = " + sqrt(x++));
    System.out.println("Wurzel von +" + (int)x + " = " + sqrt(x++));
    System.out.println("Wurzel von +" + (int)x + " = " + sqrt(x++));
    System.out.println("Wurzel von +" + (int)x + " = " + sqrt(x++));
    System.out.println("Wurzel von +" + (int)x + " = " + sqrt(x++));
    System.out.println("Wurzel von +" + (int)x + " = " + sqrt(x++));
    System.out.println("Wurzel von +" + (int)x + " = " + sqrt(x++));
    System.out.println("Wurzel von +" + (int)x + " = " + sqrt(x++));
  }
}