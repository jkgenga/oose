class Date{
  int day;
  int month;
  int year;

  public Date(int day, int month, int year) {
    this.day = day;
    this.month = month;
    this.year = year;
  }
  int dayOfWeek(){
    int m = month - 2 < 0 ? 10 + month : month - 2;
    int year = month < 3 ? this.year -1: this.year;
    int c = year / 100;
    int y = year % 100;
    int tmp = (int) ((day + (int)(2.6 * m - 0.2)
           + y + y/4 + c/4 - 2*c
           ));
     return ((tmp % 7) + 7) % 7;
  }

  Date nextDay(){
    int newDay = this.day + 1;
    int newMonth = this.month;
    int newYear = this.year;
    int maxDay = getMaxDayPerMonth();
    if (newDay > maxDay) {
      newDay = 1;
      newMonth++;
      if (newMonth > 12) {
        newMonth = 1;
        newYear++;
      }
    }
    return new Date(newDay, newMonth, newYear);
  }

  String monthAsHTML(){
    StringBuffer buf = new StringBuffer();
    buf.append("<table>\n");
    buf.append("<tr><th>Mo</th><th>Di</th><th>Mi</th><th>Do</th><th>Fr</th><th>Sa</th><th>So</th></tr>\n");
    int startDay = new Date(1,month, year).dayOfWeek();
    int weekCounter = 0;
    buf.append("<tr>");

    for (int e = 0; e < startDay - 1; e++) {
      buf.append("<td></td>");
      weekCounter++;
    }
    
    for (int d = 1; d <= getMaxDayPerMonth(); d++) {
      buf.append("<td>");
      buf.append(d);
      buf.append("</td>");
      weekCounter++;
      if (weekCounter % 7 == 0) {
        buf.append("</tr>\n");
      }
    }
    int left = 7 - weekCounter % 7;
    if (left > 0) {
      for (int l= 0; l < left; l++) {
        buf.append("<td></td>");
      }
      buf.append("</tr>\n");
    }

    buf.append("</table>\n");
    return buf.toString();
  }
  Date mothersDay(){
    Date tmp = new Date(1,5,year);
    int w = tmp.dayOfWeek() + 1;
    return new Date(15 - w, 5, year);
  }

  int getMaxDayPerMonth() {
    switch(month) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12: return 31;
      case 4:
      case 6:
      case 9:
      case 11: return 30;
      case 2: return 28;
              // if (isSchaltjahr) return 29
              // else return 28;
      default: return 0;
    }
  }

  public static void main1(String[] args) {
    Date d = new Date(17,11,2019);
    Date d2 = d.nextDay();
    d = new Date(30,11,2019);
    d2 = d.nextDay();
    d = new Date(31,12,2019);
    d2 = d.nextDay();
  }

  public static void main(String[] args) {
    System.out.println(new Date(17,11,2019).monthAsHTML());
  }

  public static void main0(String[] args) {
    System.out.println(new Date(25,1,1971).dayOfWeek());
    System.out.println(new Date(12,6,2006).dayOfWeek());
    System.out.println(new Date(12,1,2006).dayOfWeek());
    System.out.println(new Date(1,1,2000).dayOfWeek());
  }
}